/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('GeoLocation.view.main.Main', {
    extend: 'Ext.tab.Panel',
    xtype: 'app-main',

    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',
        'GeoLocation.view.main.MainController',
        'GeoLocation.util.Geolocation'
    ],
    controller: 'main',
    ui: 'navigation',
    //tabBarHeaderPosition: 1,
    titleRotation: 0,
    tabRotation: 0,
    activeTab: 0,
    items: [{
        title: 'Geo Location',
        iconCls: 'x-fa fa-map',
        itemId: 'geoLocation',
        items: [{
            xtype: 'button',
            text: 'Check weather',
            handler: 'onWeatherButtonClick'
        }, {
            xtype: 'dataview',
            store: {
                fields: [{
                    name: 'summary',
                    mapping: 'weather[0].main'
                }, {
                    name: 'description',
                    mapping: 'weather[0].description'
                }],
                proxy: {
                    type: 'jsonp',
                    url: `http://api.openweathermap.org/data/2.5/weather?lat=${commonutility.getLatitude()}&lon=${commonutility.getLongitude()}&units=metric&appid=9b59049894d42af608baf69f869b9ace`,
                    reader: {
                        type: 'json'
                    }
                },
                autoLoad: true
            },
            itemTpl: '<div class="weather-image-container"><img src="../../../resources/img/ico_cloud.png" alt="{summary}"/></div>' +
                '<div class="weather-details-container">' +
                '<div>{main.temp}&#176;</div>' +
                '<div>{summary}</div>' +
                '<div>{description}</div>' +
                '</div>'
        }]
    }],
    listeners: {
        beforerender: 'onMainViewRender'
    }
});