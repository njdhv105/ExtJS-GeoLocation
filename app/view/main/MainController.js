Ext.define('GeoLocation.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main',
    /**
     * function will fire when mainview is rendered of initialize.
     * @param {Ext.tab.Panel}
     */
    onMainViewRender: function (tabpanel) {
        var className = '',
            geo;
        if (Ext.isClassic) {
            className = 'GeoLocation.util.Geolocation';
        } else if (Ext.isModern) {
            className = 'Ext.util.Geolocation';
        }
        if (className) {
            geo = Ext.create(className, {
                autoUpdate: false,
                listeners: {
                    locationupdate: function (geo) {
                        debugger;
                        commonutility.setLatitude(geo.getLatitude());

                        commonutility.setLongitude(geo.getLongitude());

                        Ext.Msg.alert('Success', `New latitude: ${commonutility.getLatitude()} <br>New Longitude: ${commonutility.getLongitude()}`);

                        console.log(`New latitude: ${commonutility.getLatitude()} <br>New Longitude: ${commonutility.getLongitude()}`);
                    },
                    locationerror: function (geo, bTimeout, bPermissionDenied, bLocationUnavailable, message) {
                        if (bTimeout) {
                            Ext.Msg.alert('Error', 'Timeout occurred.');
                        } else {
                            Ext.Msg.alert('Error', 'Error occurred');
                        }
                    }
                }
            });
            geo.updateLocation();
        } else {
            Ext.Msg.alert('Error', 'No class found');
        }
    },
    /**
     * this event will fire on click of check weather button
     * @param {Ext.button.Button}
     */
    onWeatherButtonClick: function (button) {
        var store = button.up('#geoLocation').down('dataview').getStore();

        store.load({
            url: `http://api.openweathermap.org/data/2.5/weather?lat=${commonutility.getLatitude()}&lon=${commonutility.getLongitude()}&units=metric&appid=9b59049894d42af608baf69f869b9ace`,
        });
        button.setDisabled(true);
    }
});