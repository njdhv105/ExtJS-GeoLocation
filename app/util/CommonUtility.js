Ext.define("GeoLocation.util.CommonUtility", {
    singleton: true,
    alternateClassName: 'commonutility',
    config: {
        latitude: 0,
        longitude: 0
    },
    constructor: function (config) {
        this.initConfig(config);
    }
});